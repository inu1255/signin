#!/usr/bin/env python
# coding=utf-8

"""
京东手机流量
"""
import requests
import logging,json

logging.basicConfig(level=logging.DEBUG,
                format='%(asctime)s %(levelname)s\t%(filename)s[line:%(lineno)d] %(message)s',
                datefmt='%a,%Y-%m-%d %H:%M:%S',
            )

class Base(object):
    title = False
    url = ""
    method = "post"
    headers = {}
    cookies = {}
    data = {}
    def __init__(self,*arg,**kargv):
        params = self.getParams(self.getTitle(),*arg,**kargv)
        self.parseParams(params)
        resp = requests.request(self.method, self.url, data=self.data, headers=self.headers, cookies=self.cookies, verify=True)
        try:
            data = self.check(resp)
            if data:
                self.success(resp)
            else:
                self.failure(resp)
        except Exception as e:
            logging.warning(e)
            self.failure(resp)
    def getTitle(self):
        if self.title:
            return self.title
        return type(self).__name__
    def getParams(self,title,*arg,**kargv):
        return kargv
    def parseParams(self,params):
        pass
    def check(self,resp):
        pass
    def success(self,resp):
        if len(resp.text)>243:
            logging.debug(resp.text[:81]+"..."+resp.text[-81:])
        else:
            logging.debug(resp.text)
        logging.info("[%s]成功",self.getTitle())
    def failure(self,resp):
        if len(resp.text)>243:
            logging.info(resp.text[:81]+"..."+resp.text[-81:])
        else:
            logging.info(resp.text)
        logging.info("[%s]失败:",self.getTitle())

class JDMobileData(Base):
    """
    https://fbank.m.jd.com/index/index.action?_ts=1494138303148&resourceType=jdapp_share&resourceValue=CopyURL&utm_source=iosapp&utm_medium=appshare&utm_campaign=t_335139774&utm_term=CopyURL&sid=33458cd1aed43a6b1f8d7b5e116278cd
    """
    title = "京东流量站签到"
    url = "https://fbank.m.jd.com/api.json?functionId=fBankSign&body=%7B%7D&_r=1494326081207"
    headers = {"Cookie":'shshshfpa=732f0cac-73e8-bee9-7ba3-89b32105baca-1494178326; shshshfpb=18baaa93a52e8472c977811ab74d743077ac5b858f7258e26590f5a170; TrackerID=5IkVk36FIta2RUZUMobL5k_1acLhuEeLYlpiiLTVLbTqxSK07XmHyFIAOMfnAg2JmUa1pHBuLoPGRqjxlgi2PN_T3hG2NokeHE01ZVwLwayP6hX8y5rQ28QWfLKxy1zR; pinId=ABpAHZUANeWehyQCw_pnnLV9-x-f3wj7; pt_key=AAFZD1o9ADDhJqRKYua0fRvGdWcH1RUsk47ebiQEXOABpao0kuh2HTHWIJsZNYk-kvf2qkAUP_I; pt_pin=jd_4b97e4f23a2c1; pt_token=wqw8gir8; pwdt_id=jd_4b97e4f23a2c1; whwswswws=8EPQOf%2BAkrTBQzax%2B4EzTi1Fnm%2BO%2BfE8Y%2Fdv73kzLbacuj%2FllQJqgg%3D%3D; abtest=20170508013246231_49; USER_FLAG_CHECK=d87dd31ca7d70bc18792999b164319a4; returnurl="http://luck.m.jd.com/index/index.action?activityId=4001&businessType=-118&sid=43773fee712ed083d53f283d51ef2d23"; user-key=6a6e4f0e-1c51-4f4d-8a03-aee7a327fb3f; cn=0; ipLoc-djd=1-72-2799-0; ipLocation=%u5317%u4EAC; unpl=V2_ZzNtbREHFEEnCERWKR4JAWJTGlsSVUcVcgBABHgRXgZhBBFUclRCFXMUR1BnGFQUZwoZXUFcQRdFCHZXfBpaAmEBFl5yBBNNIEwEACtaDlwJBhRVQ15LEH0KRlFLKV8FVwMTbUJXRxR8CENQchpsNWAzIm1AU0MUdQ52VUsYbEczXxtbQlBAFzgIRlB6EFwAYwoRbUNnQA%3d%3d; CCC_SE=ADC_eIGS9EmFT4Y1P0MIbZJZtWLmDY%2fntSXRuzi%2fIwcmTHqy7d0PPQ0NELgB34qCF85DmG%2bwstoFN5ZS5b3%2fiwedaVK3M5%2foOk2NJz6Xxrbp5cV1pw7n3t2Zprtzhd3bn%2baWt02to%2b9VPsYsk%2fYaZnD%2bAXOY4VAukBoVGZxUq85SUTcMv1xr%2bO%2f500NnQWGNXGe0WHQSqy%2fdvlVxvZb%2fX61RiYZaonH4zpaqHtmeOmxwul1Df3vQWLIuS%2bZIN8AAd9%2bXpJ0cGUl%2by0YLerx9WSJxrk5UPnPf03E1e4i6d6iE2NXlXqYq1oWjGKGFKp6XMRgKCpYLo%2bx17WR3NygJ0C9DRzlCen5sXuNU7gADBZj4PLG44E8ASTrhlmwTA2LWI48Fp1xj8kncqEG%2bdYhlsm6W6XREizVz8dmMifyp7huOL5joFC0uf0yXcVy3sis%2br3EjBphAdRhNAx2cgFkVRhcpo65tzQIZ9twYLx43EfXdfr46QUw%2bj%2f3RJC9SJU19fUtV; mt_xid=V2_52007VwMSVlxRUl8bQBpsBmJQE1VaCwVGHkwfCxliV0VRQQgFW0xVGAlVYlMbUVkKBVhKeRpdBWEfE1ZBWVpLH0ASXAZsBhZiX2hSah9JHF0GZwUUVm1bVVM%3D; 3AB9D23F7A4B3C9B=TMRLGY4FDCSPEXDVFOFH4FTCVLXCZFEC5DOC4ACKVPU7WBPXVWLUJI4GQDMKZJ2SLQOESUK4U5DEWE6ODMOZIMY2QU; sid=33458cd1aed43a6b1f8d7b5e116278cd; __jda=122270672.1150814582.1480338154.1494324754.1494325823.10; __jdb=122270672.2.1150814582|10.1494325823; __jdv=122270672|iosapp|t_335139774|appshare|CopyURL|1494325878137; __jdc=122270672; mobilev=html5; __jdu=1150814582; mba_muid=1150814582; mba_sid=14943258234807666073074845077.2'}
    def check(self,resp):
        v = resp.json()
        if v["code"]=="0":
            return True

class CmccSendMsg(Base):
    title = "移动掌上营业厅发短信"
    url = "http://218.205.252.24:18080/scmccClient/action.dox"
    headers = {"Content-Type":"application/x-www-form-urlencoded","platform":"iphone"}
    def parseParams(self,params):
        telphone = "18782071219"
        data = {
            "auth"       :"yes",
            "appKey"     :"00011",
            "md5sign"    :"78752DA9F38294956B851D72487D68B1",
            "internet"   :"WiFi",
            "sys_version":"10.3.1",
            "screen"     :"1242*2208",
            "model"      :"iPhone",
            "imei"       :"AE7CB612-EBFE-4E9E-98A3-B7EADDE4D0EF",
            "deviceid"   :"AE7CB612-EBFE-4E9E-98A3-B7EADDE4D0EF",
            "version"    :"3.3.2",
            "msgId"      :"",
            "jsonParam"  :'[{"dynamicURI":"/SmsPwdVerifyLogin","dynamicParameter":{"method":"sendSSOSmsCode","number":"%s","busiNum":"SSO_SENDSMS",},"dynamicDataNodeName":"sendSSOSmsCode_node"}]' % telphone
        }
        self.data = data
    def check(self,resp):
        v = resp.json()
        if v["sendSSOSmsCode_node"]["resultCode"] =="1":
            logging.info("发送验证码成功")
            return True

class CmccSigin(Base):
    title = "移动掌上营业厅签到"
    url = "http://218.205.252.24:18081/scmccCampaign/signCalendar/sign.do"
    # headers = {'Cookie':'JSESSIONID=8PLs3YmqyCEoAwEAGNyEA0Ok0uJfZUaiHbhG4U3KRVpQ_hlLH8Oz!930052221; WT_FPC=id=2c7ab851d7d5ba7c24f1494228982762:lv=1494327593446:ss=1494327567638; smsCityCookie=11; SmsNoPwdLoginCookie=011A1D4EA3AFAACA23BECFA6DDF27FAC; cstamp=1494224925166'}
    data = {"SSOCookie":"011A1D4EA3AFAACA23BECFA6DDF27FAC"}
    def check(self,resp):
        v = resp.json()
        if v["result"]["code"]==0:
            return True

class CmccSendBigWheel(Base):
    title = "移动掌上营业厅大转盘"
    url = "http://218.205.252.24:18081/scmccCampaign/dazhuanpan/dzpDraw.do?t=0.32153932745138225"
    headers = {"Content-Type":"application/x-www-form-urlencoded","platform":"iphone"}
    data = {"SSOCookie":"011A1D4EA3AFAACA23BECFA6DDF27FAC"}
    def check(self,resp):
        v = resp.json()
        if v["dzpDraw"]["code"]=="0":
            return True

# 无效
class TaoBaoCoins(Base):
    title = "淘宝淘金币"
    url = "https://acs.m.taobao.com/h5/mtop.matrixexchange.wireless.getallcoin.receive/1.0/?appKey=12574478&t=1494329032364&sign=8f0a5ce20a60abd7dedc0c3ca70b7d16&api=mtop.matrixexchange.wireless.getAllCoin.receive&v=1.0&ecode=1&type=jsonp&dataType=jsonp&callback=mtopjsonp7&data=%7B%7D"
    headers = {'Cookie':'miid=270980908956411556; thw=cn; tk_trace=oTRxOWSBNwn9dPyscxqAz9fIO75ATfFmwiEpsGR4%2Fu%2B%2BvwfzW4tqcIIPcCgM1pl0FFatgTDkBPXxeSWdE6vN%2FyyEbtOH19lc2TiWR1ww2iwIpNl1OGjcXTFYmvNM%2Bpidu%2FTWMubry3kmQ2SDqAZDUPCS0ZvrE3J8UIah0cXfVP3kxyTOK%2B4tNSCIOwHBEeNkNCt8gZh3gHtW5ZyCcCVbzUyS5lyHqvfrMDPel33lV%2B0v7bjzXv%2BgkU0a6Cj1ARwFSFgsLOoBLTppDTGDaTkromw93Gcue1CsTfzn9u83F957MEdraWphejC5h9%2FLRgJSMGc7y2pv56N86Dy0Al%2BpL5W00sd6d4RZ%2BoWxl7YbIozAmuNXxebVPi9ePg42wes5OgU%2BnM%2FKwYFfjKk2D80HXdlScLAME3KNCGp1XvOp%2BuNiczm548dAeWG50Iu7ij8f3pIamMovlp6gg9clgo3g9L29apmqGCGWXQE%2FL3L8gaz7nkUjHiUb5MOADgdr93HnYx0kz53esbJTXdMrp7tfB5ETtUpUZvIUwkA5FfpdWr6bHlFC1Xh9a%2F%2F414SgslYbauPmjyhYvEsBNuhaylVP5dB89%2FHXmvdloVxquoQJPnGtXEBfXvL%2BgdZJ6sIQF%2FU8V2WNDTwysSAZv0WewW%2F7J0LWCc47Typ2GfrfeCU9yFRFDMonnisBWyCfsyZVRMig%2B7KNdVEjoCcFwrJs2T1qNfur1Jfxw%2F6%2BC3lpbN%2FiCv8Ex3t8LTmvIcmJgAgIuwZ9hSoyYTqbMSAUzxBgOPtyOZ67NDmRUJyHdj4mc6IqBogqNaJqmstVEA%3D%3D; cookie2=1c43863210b3f3472e27ce23b91cb1fc; tg=0; v=0; linezing_session=GZAKa5PP1q68iwGXId8CTofn_1494321168980ZaAZ_1; _tb_token_=FZXOyAUfqsVp; _m_user_unitinfo_=unit|unsz; _m_unitapi_v_=1492572565585; mt=ci=-1_0; cna=dRnDEMDKewcCAavWtVjxRLbT; _m_h5_tk=e54f0736a9549dd17f9984a6ee1c7be5_1494425848369; _m_h5_tk_enc=8b73ae86f66907383c7faf103e85e692; abt=a; ockeqeudmj=gOn9Fbk%3D; munb=1849497592; WAPFDFDTGFG=%2B4cMKKP%2B8PI%2BtRUAk4fL%2FIxTjg%3D%3D; _w_app_lg=19; uc3=sg2=VW22vyt4pDBRCAHCcswDtOBHwH9lA%2FLHf%2FdQw%2BtGK7Y%3D&nk2=3Wx1xw%2B8&id2=UonfPo7OQpFHig%3D%3D&vt3=F8dARVDSWG50tE50Cek%3D&lg2=Vq8l%2BKCLz3%2F65A%3D%3D; uc1=cookie14=UoW%2BvPp441Vx6A%3D%3D&cookie21=VT5L2FSpccLuJBreK%2BBd&cookie15=V32FPkk%2Fw0dUvg%3D%3D; lgc=%5Cu9759vs%5Cu9ED8; uss=U7PLR6atQvJOs%2BS1yEIOSEqLB0cEs4ue0088AYSn2jvKwuW4guZKbOig; tracknick=%5Cu9759vs%5Cu9ED8; sg=%E9%BB%982c; cookie1=UNDVe%2B3ock3lfFo08MrOtRJnjD63R%2BV9pxkA2xuFO6M%3D; ntm=0; unb=1849497592; skt=d6eafd0f8dac6a6c; t=a64f567cc5b81c153555162dfcaf6434; _cc_=URm48syIZQ%3D%3D; _nk_=%5Cu9759vs%5Cu9ED8; _l_g_=Ug%3D%3D; cookie17=UonfPo7OQpFHig%3D%3D; l=AhUVRV6SHMipWlDCne-YL1jhpQrPEskk; isg=AmdnSpuF8m2Wj3eoiVtVAoRH9p1-UTvOWKolxznUg_YdKIfqQbzLHqUqOJJJ'}
    method = "get"
    def check(self,resp):
        s = resp.text
        v = json.loads(s[12:-1])
        if v["ret"][0]==u"SUCCESS::调用成功":
            return True

# 无效
class LiquSignin(Base):
    title = "利趣味网签到"
    url = "http://www.liqu.com/QuBi/Qiandao?is_ajax=ajax"
    headers = {'Cookie':'UM_distinctid=15bf0e7d0da78f-09e46c67013434-153d655c-fa000-15bf0e7d0decd5; lqsid=qx0g4teznswiuhz0tfgbmejc; tg_info=t=%E5%A4%96%E7%AB%99&outid=c1396aed-a36e-412b-8ec8-6a80d28ff4b8; _liquweb=4C03519E65085F34128C0F975C0D3B8C6DA32B4CCDFC7BDC00FB1590E467D1D2E78B1211E7BA8F1E8BEA78EB3346D43669F74E274673C0577FF43E597687EC0A257ED47D28936FF50344B4CA0E226A0104EE91B973984236C944489A3F473C712E908A2BBF8158F29A62ECEE83DDE86C9265C0C6AB20BF23C486B0B9ACFCE022C6EF2D58C12D1566E8D1910F143E457E3727D68686DE7308F191D28E90C8F2513B4C92234DFC20B471DA2B5D3B2F2511EEC3471D92CEC6EB0B47DA92EA15CCFD375174222E242411AABCA3B9629D198A6DDD721F8B1ED22A87934DD61CF0B84EE9583F0ABC8A68D906B7015ACAEA347EB8EA0DF077E8A6A6A85F01A1E1C46812EB8DB21319A99C36678BBAE67AC19D16D793107547F3D995950C2D299E24613A0EE86EAC92200D33B3F3B42E5DAC59D2E2203968C2161B6ACBC39F66637FD425F8B96BD2281831DE26511F37EE20E7A99E26F5E69CA6CD8EAA41C94DDB8A7351248A023E; _user={"nick":"929909260%40qq.com","alipay":"","uid":2689985,"loginType":0}; haslogin=true; Hm_lvt_68b265d5f88010df636291fcfcc6de50=1494395376,1494408328,1494408339; Hm_lpvt_68b265d5f88010df636291fcfcc6de50=1494408466; CNZZDATA1258115878=581592417-1494391355-null%7C1494407561'}
    method = "get"
    def check(self,resp):
        v = resp.json()
        if v["code"]==200:
            return True

# 无效
class WeiphoneSignin(Base):
    title = "威锋网签到"
    url = "http://fengplus.feng.com/index.php?r=api%2FuserSign%2FuserDailySign"
    headers = {
        'User-Agent': 'WPForumPortal/4.8 (iPhone; iOS 10.3.1; Scale/3.00)',
        'Cookie': 'NSC_gfohqmvt.gfoh.dpn=ffffffffc3a0d98245525d5f4f58455e445a4a423660; PHPSESSID=633742q3q17tmgr5pr82r086s6; useruniquecook=5912dfc3d3e5e'
    }
    data = "data=eyJhcHBfa2V5IjoiZTMyOWIyNzk0Mzk2MWNjYWI5ZGM3YThhYzJlZGI2NjYiLCJ2ZXJpZnkiOiJjZjU0MDVhNjg0NThjNjA1YWM4OTkxMWJiMWZhNWRhZiIsImVuY3J5cHRfZGF0YSI6Ik1heTlKVmxuUTdvcG9xN2IzU2xGS3U4SlZWb2lkRnMwQ052elo3aml1QUZOZTlBOFEwU1wva3dsOFwvQm02MXJtbUsxUjVBT2twODJFMVVEK2FFY2xnVXZcL0VmU1pqeHhBWjNDUTFcL3p0SitKejJ4SENLQ2R4VWN1XC9yUGdlSUlOQWFoYUpwM3RoZkVIU3d2QlJ4VksrK1ZNWlVzblpQYytWR0t0ZDJRaExKVk9DOEdaUFwvRkFrT2FraWYwVFpCVTJKXC9ZOHRmZ2NvR2p2QUl4cUk1NVlLR1Z0TFRiZ2tnTmFUT0Y4eDRKN2tNV0xiXC9xN0tKOEk5S29LcHkyOWFTeFwvRFlVSkptc2RWUlwvdTVlTWxLb0lIU0ZPZjd1RUFFNEZQR2lLNmpWQ0VhaXREWGJtT3hCdDlqU1VneEd0ZWdsVVwveHZXR3BcL0pVN3F6WXRjK3JYT0RVOVwvaVpscWFzWW1mRnpqY0xRSGxBQWorbWM4Z2hEb1prMmUzemo4dWp3c1NUb2ZYcGhEV0NKd00rWnZoaUo5REJEdXZiVE5CMGdVZHR1emkwQkdqdnYwOHNuY293b21MR293NHV5RkFDZEUzczlJYUExNlBHaHY4c3BPSzhUdW14ZFdjclVITGNlZ3ViZFhyekRUd0g1djhNTTZZdzhENkczd2hRRHhDUGZRR3Zna0JqSHlEZ3ZHZkhqZXZSeFM2SldWdkNhRU4xZDdIeVh3ZmNBOGJiWEp5S0lhR0UzcVhHSVlXaVwvVlNCVHcwV0Z1YU11K09MeCtMUFRNbnZoNzByb2hSV2tPMUxvKzlac21UaTE3S1J0bm9TeDhSZ2xMOUpRN3VCMlRjb3h0eUYzb1o4QlU0WkJVenNwY2ljY0xEYkcrakI3Z25lSk43WG5Lclh6XC9HXC9oRkV3QkgzMGZPNlpnUHlQa20rT25Yb2NiT0hiUDg3MXJseW9HZ2FqeldrcnNpbUJHeFwveEc5NkpCVENGbWk0dHVmSWpmVGlZNXMyaXg5UW1CcVdtc0xnUDBqa3RxbW5FOFh6XC9wdnRIOTRYRDl3Qk9wOExRaUNCSnY0ZlwvZEE0QVpweFo4MDZ4aVlVdTJHUXZOVTlIQmZva2FHVkVHNUw4UVBtdkc4d1NxZkRsODFHa2dIN0dHSENCZks5cWdrZWFXeEpzamZnN1dRZHNQYkNkS2hoNm1EK1dqK2RxTzdvNFQyM0grOUJyV0IzaHMramNQeVhoTkRmY214MnhHRXMrZlN4T0dRXC93WDNCemx6dmxmRW10OG50R2V3TmhqS0N3R1Z6TE1tQVVXck5BUGl3b0dCdElUcjUwcXFvZVwvVXM0RFV3Mm16WFBRZm1wT0I5Z04yYk1cL25OSFJYNHp1TDBUXC9nZDZaVWFRcDhXZjFFQmlZVzJRU0c5SGdlNXgydVZlYUtrdmI5M1RPQ3VRVnMxaXY4WEUrOXRwc2pUN2tXNDNxaG5nRDBMQTFjNkE5SVJBN0VVa2FQYUl6QmhQWWVRcVEyWkpZbjVIMVJuTVpnYzVGazZQd0FhWXZMaHFHY05kODVXWHp5SnlLTE80SWJiUGlYR0lET2dSODRZYWVhamlpSFZyaTkzSXVJcnZzYTZ6eURwQklURDVLcHBvTmdCMkhkdU9VU3JPRUlyRnpKSk5SbXB4K0pwZVwvWk9cL1wvYmN6R0pJeFBvS1I1Qlo4UW9tUT09In0%3D"
    def check(self,resp):
        v = resp.json()
        if v["success"]==0:
            logging.info(v["info"])
            return True

# 无效
class Jifen2345Signin(Base):
    """
    http://jifen.2345.com/index.php
    """
    title = "2345技术员签到"
    url = "http://jifen.2345.com/jifen/every_day_signature_new.php?sign_token=14df2249c8e842b3f8adb5cd8662bc97&channel=1"
    headers = {
        'Cookie':'sts=0; lc2=56294; wc=60912; wc_n=%25u65B0%25u90FD; lc=56294; common_cookie_special_key=%7B%22J_trig_close_new%22%3A%7B%22e%22%3A1494453452%7D%7D; PHPSESSID=76d38df2d87e7f2a72481ac3cf574025; remember=1; uid=17493250; name_ie=%2549%2556%2555%2556%2550%2548%2555%2549%2550%2549%2557; site_str_flag=2; need_modify_name=0; I=i%3D16937211%26u%3D17493250%26n%3Dinu1255%26m%3D0%26t%3D1494410437.63973700%26s%3D68aae544f768d4606b845fbf2aa80c4f%26v%3D1.1; sec_login_code=97e7290e24a8abbe95033bfbe9687cd6%7C1494410495; last_use=1494410999; score=55'
    }
    method = "get"
    def check(self,resp):
        if resp.text.startswith("succ"):
            return True

# 无效
class Wz148Signin(Base):
    """
    http://www.148wz.com/M_commendcode.asp
    """
    title = "148网赚之家签到"
    url = "http://www.148wz.com/M_accounts.asp"
    headers = {
        'Cookie':'www.918178.com.cn=yyq148wzJwei; UM_distinctid=15bf1de69e87fa-091bf3210046df-153d655c-fa000-15bf1de69e9876; yunsuo_session_verify=0bb026cd4604d3689feb05fa9e2c8230; ASPSESSIONIDQADSBRSB=GBIFGCHBJMMJAEJOEFLGBFCK; www%2E148wz%2Ecom=md5apassword=725b2082666cd9ee9ef961c6e17edba0&UserName=inu1255; CNZZDATA2231678=cnzz_eid%3D367254589-1494409782-http%253A%252F%252Fwww.qdzq.lingw.net%252F%26ntime%3D1494409951'
    }
    method = "get"
    def check(self,resp):
        return True

if __name__=="__main__":
    JDMobileData()
    CmccSendBigWheel()
    CmccSigin()
    # CmccSendBigWheel()
    # TaoBaoCoins()
    pass
    