#!/usr/bin/env python
# coding=utf-8

import schedule
from signin import jobs
import time
import logging

logging.basicConfig(level=logging.INFO,
    format='%(asctime)s %(levelname)s\t%(filename)s[line:%(lineno)d] %(message)s',
    datefmt='%a,%Y-%m-%d %H:%M:%S',
)

schedule.every().day.at("10:30").do(jobs.JDMobileData)
schedule.every().day.at("10:31").do(jobs.CmccSendBigWheel)
schedule.every().day.at("10:32").do(jobs.CmccSigin)

while True:
    schedule.run_pending()
    time.sleep(1)